```
$ gcc aa30zero_part1.c -o part1
$ g++ aa30zero_part2.cpp -o part2
$ ./part1 /dev/cu.usbmodem1461 fq7050000 sw100000 frx2 | awk -F, -f aa30zero_part3.awk | ./part2 > mydata.txt
$ gnuplot aa30zero_part4.plt
```

![alt txt](http://spinorlab.matrix.jp/en/wp-content/uploads/2017/12/Screen-Shot-2017-12-23-at-9.20.24.png)